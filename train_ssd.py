"""Train SSD"""
import logging
import os
import time

import gluoncv as gcv
import mxnet as mx
from mxnet import autograd
from mxnet import gluon
from mxnet import nd

gcv.utils.check_version('0.6.0')
from gluoncv.data.batchify import Tuple, Stack, Pad
from gluoncv.data.transforms.presets.ssd import SSDDefaultTrainTransform
from gluoncv.data.transforms.presets.ssd import SSDDefaultValTransform

from mxnet.contrib import amp

from models.ssd import get_model
from utils.base_trainer import TrainerABC

try:
    import horovod.mxnet as hvd
except ImportError:
    hvd = None

try:
    from nvidia.dali.plugin.mxnet import DALIGenericIterator

    dali_found = True
except ImportError:
    dali_found = False


class SSDTrainer(TrainerABC):
    base_name = 'ssd'
    model_maker = get_model

    def _add_additional_arguments(self, parser):
        pass

    def _train_internal(self, train_data, val_data, eval_metric):
        """Training pipeline"""
        net = self.net
        ctx = self.ctx
        args = self.args
        net.collect_params().reset_ctx(ctx)

        if args.horovod:
            hvd.broadcast_parameters(net.collect_params(), root_rank=0)
            trainer = hvd.DistributedTrainer(
                net.collect_params(), 'sgd',
                {'learning_rate': args.lr, 'wd': args.wd, 'momentum': args.momentum})
        else:
            trainer = gluon.Trainer(
                net.collect_params(), 'sgd',
                {'learning_rate': args.lr, 'wd': args.wd, 'momentum': args.momentum},
                update_on_kvstore=(False if args.amp else None))

        if args.amp:
            amp.init_trainer(trainer)

        # lr decay policy
        lr_decay = float(args.lr_decay)
        lr_steps = sorted([float(ls) for ls in args.lr_decay_epoch.split(',') if ls.strip()])

        mbox_loss = gcv.loss.SSDMultiBoxLoss()
        ce_metric = mx.metric.Loss('CrossEntropy')
        smoothl1_metric = mx.metric.Loss('SmoothL1')

        # set up logger
        logging.basicConfig()
        logger = logging.getLogger()
        logger.setLevel(logging.INFO)
        log_file_path = args.save_prefix + '_train.log'
        log_dir = os.path.dirname(log_file_path)
        if log_dir and not os.path.exists(log_dir):
            os.makedirs(log_dir)
        fh = logging.FileHandler(log_file_path)
        logger.addHandler(fh)
        logger.info(args)
        logger.info('Start training from [Epoch {}]'.format(args.start_epoch))
        best_map = [0]

        for epoch in range(args.start_epoch, args.epochs):
            while lr_steps and epoch >= lr_steps[0]:
                new_lr = trainer.learning_rate * lr_decay
                lr_steps.pop(0)
                trainer.set_learning_rate(new_lr)
                logger.info("[Epoch {}] Set learning rate to {}".format(epoch, new_lr))
            ce_metric.reset()
            smoothl1_metric.reset()
            tic = time.time()
            btic = time.time()
            if not args.debug:
                net.hybridize(static_alloc=True, static_shape=True)

            for i, batch in enumerate(train_data):
                data = gluon.utils.split_and_load(batch[0], ctx_list=ctx, batch_axis=0)
                cls_targets = gluon.utils.split_and_load(batch[1], ctx_list=ctx, batch_axis=0)
                box_targets = gluon.utils.split_and_load(batch[2], ctx_list=ctx, batch_axis=0)

                with autograd.record():
                    cls_preds = []
                    box_preds = []
                    for x in data:
                        cls_pred, box_pred, _ = net(x)
                        cls_preds.append(cls_pred)
                        box_preds.append(box_pred)
                    sum_loss, cls_loss, box_loss = mbox_loss(
                        cls_preds, box_preds, cls_targets, box_targets)
                    if args.amp:
                        with amp.scale_loss(sum_loss, trainer) as scaled_loss:
                            autograd.backward(scaled_loss)
                    else:
                        autograd.backward(sum_loss)
                # since we have already normalized the loss, we don't want to normalize
                # by batch-size anymore
                trainer.step(1)

                if not args.horovod or hvd.rank() == 0:
                    local_batch_size = int(args.batch_size // (hvd.size() if args.horovod else 1))
                    ce_metric.update(0, [l * local_batch_size for l in cls_loss])
                    smoothl1_metric.update(0, [l * local_batch_size for l in box_loss])
                    if args.log_interval and not (i + 1) % args.log_interval:
                        name1, loss1 = ce_metric.get()
                        name2, loss2 = smoothl1_metric.get()
                        logger.info('[Epoch {}][Batch {}], Speed: {:.3f} samples/sec, {}={:.3f}, {}={:.3f}'.format(
                            epoch, i, args.batch_size / (time.time() - btic), name1, loss1, name2, loss2))
                    btic = time.time()

            if not args.horovod or hvd.rank() == 0:
                name1, loss1 = ce_metric.get()
                name2, loss2 = smoothl1_metric.get()
                logger.info('[Epoch {}] Training cost: {:.3f}, {}={:.3f}, {}={:.3f}'.format(
                    epoch, (time.time() - tic), name1, loss1, name2, loss2))
                if (epoch % args.val_interval == 0) or (args.save_interval and epoch % args.save_interval == 0):
                    # consider reduce the frequency of validation to save time
                    (map_name, mean_ap), fps = self.validate(net, val_data, ctx, eval_metric)
                    val_msg = '\n'.join(['{}={}'.format(k, v) for k, v in zip(map_name, mean_ap)])
                    logger.info('[Epoch {}] Validation: \n{}'.format(epoch, val_msg))
                    logger.info(f'[Epoch {epoch}] Validation fps: {fps:.2f}')
                    current_map = float(mean_ap[-1])
                else:
                    current_map = 0.
                self.save_params(best_map, current_map, epoch)

    @classmethod
    def _make_dataloader(cls, net,
                         train_dataset, val_dataset,
                         batch_size, ctx, args):
        """Get dataloader."""
        train_shape, val_shape = args.data_shape
        # use fake data to generate fixed anchors for target generation
        with autograd.train_mode():
            _, _, anchors = net(mx.nd.zeros((1, 3, train_shape, train_shape), ctx))
        anchors = anchors.as_in_context(mx.cpu())
        batchify_fn = Tuple(Stack(), Stack(), Stack())  # stack image, cls_targets, box_targets
        train_loader = gluon.data.DataLoader(
            train_dataset.transform(SSDDefaultTrainTransform(train_shape, train_shape, anchors)),
            batch_size, True, batchify_fn=batchify_fn, last_batch='rollover', num_workers=args.num_workers)
        val_batchify_fn = Tuple(Stack(), Pad(pad_val=-1))
        val_loader = gluon.data.DataLoader(
            val_dataset.transform(SSDDefaultValTransform(val_shape, val_shape)),
            batch_size, False, batchify_fn=val_batchify_fn, last_batch='keep', num_workers=args.num_workers)
        return train_loader, val_loader


if __name__ == '__main__':
    trainer = SSDTrainer()
    trainer.init()
    if trainer.args.test:
        trainer.test()
    else:
        trainer.train()
