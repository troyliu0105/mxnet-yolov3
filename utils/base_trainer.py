import abc
import argparse
import os
import time
import warnings
from typing import Optional

import mxnet as mx
import mxnet.autograd
import tqdm
from gluoncv import data as gdata
from gluoncv import utils as gutils
from gluoncv.utils.metrics.coco_detection import COCODetectionMetric
from gluoncv.utils.metrics.voc_detection import VOC07MApMetric
from mxnet import gluon
from mxnet.contrib import amp

try:
    import horovod.mxnet as hvd
except ImportError:
    hvd = None


class TrainerABC(object, metaclass=abc.ABCMeta):
    base_name = 'detection'
    model_maker: Optional[callable] = None

    def __init__(self):
        self.args = None

    def _parse_arguments(self):
        parser = argparse.ArgumentParser(description='Train networks.')
        parser.add_argument('--network', type=str, default='mobilenet1.0',
                            help="Base network name which serves as feature extraction base.")
        parser.add_argument('--data-shape', nargs='+', type=int, default=[416],
                            help="train and test image-sizes," +
                                 "Training is with random shapes from (320 to 608).")
        parser.add_argument('--batch-size', type=int, default=8,
                            help='Training mini-batch size')
        parser.add_argument('--dataset', type=str, default='voc',
                            help='Training dataset. Now support voc.')
        parser.add_argument('--num-workers', '-j', dest='num_workers', type=int,
                            default=4, help='Number of data workers, you can use larger '
                                            'number to accelerate data loading, if you CPU and GPUs are powerful.')
        parser.add_argument('--gpus', type=str, default='0',
                            help='Training with GPUs, you can specify 1,3 for example.')
        parser.add_argument('--epochs', type=int, default=150,
                            help='Training epochs.')
        parser.add_argument('--resume', type=str, default='',
                            help='Resume from previously saved parameters if not None. '
                                 'For example, you can resume from ./yolo3_xxx_0123.params')
        parser.add_argument('--start-epoch', type=int, default=0,
                            help='Starting epoch for resuming, default is 0 for new training.'
                                 'You can specify it to 100 for example to start from 100 epoch.')
        parser.add_argument('--lr', type=float, default=0.001,
                            help='Learning rate, default is 0.001')
        parser.add_argument('--lr-mode', type=str, default='step',
                            help='learning rate scheduler mode. options are step, poly and cosine.')
        parser.add_argument('--lr-decay', type=float, default=0.1,
                            help='decay rate of learning rate. default is 0.1.')
        parser.add_argument('--lr-decay-period', type=int, default=0,
                            help='interval for periodic learning rate decays. default is 0 to disable.')
        parser.add_argument('--lr-decay-epoch', type=str, default='120,135',
                            help='epochs at which learning rate decays. default is 160,180.')
        parser.add_argument('--warmup-lr', type=float, default=0.0,
                            help='starting warmup learning rate. default is 0.0.')
        parser.add_argument('--warmup-epochs', type=int, default=3,
                            help='number of warmup epochs.')
        parser.add_argument('--momentum', type=float, default=0.9,
                            help='SGD momentum, default is 0.9')
        parser.add_argument('--wd', type=float, default=0.0005,
                            help='Weight decay, default is 5e-4')
        parser.add_argument('--log-interval', type=int, default=100,
                            help='Logging mini-batch interval. Default is 100.')
        parser.add_argument('--save-prefix', type=str, default='save',
                            help='Saving parameter prefix')
        parser.add_argument('--save-interval', type=int, default=10,
                            help='Saving parameters epoch interval, best model will always be saved.')
        parser.add_argument('--val-interval', type=int, default=10,
                            help='Epoch interval for validation, increase the number will reduce the '
                                 'training time if validation is slow.')
        parser.add_argument('--mixup', action='store_true',
                            help='whether to enable mixup.')
        parser.add_argument('--no-mixup-epochs', type=int, default=20,
                            help='Disable mixup training if enabled in the last N epochs.')
        parser.add_argument('--seed', type=int, default=3355411,
                            help='Random seed to be fixed.')
        parser.add_argument('--num-samples', type=int, default=-1,
                            help='Training images. Use -1 to automatically get the number.')
        parser.add_argument('--syncbn', action='store_true',
                            help='Use synchronize BN across devices.')
        parser.add_argument('--no-wd', action='store_true',
                            help='whether to remove weight decay on bias, and beta/gamma for batchnorm layers.')
        parser.add_argument('--amp', action='store_true',
                            help='Use MXNet AMP for mixed precision training.')
        parser.add_argument('--horovod', action='store_true',
                            help='Use MXNet Horovod for distributed training. Must be run with OpenMPI. '
                                 '--gpus is ignored when using --horovod.')
        parser.add_argument('--debug', action='store_true', help='Enable debug mode: 1)disabling the hybridize.')
        parser.add_argument('--postfix', type=str, default='', required=False, help='Append to name')
        parser.add_argument('--test', action='store_true', required=False, help='Validate model')
        self._add_additional_arguments(parser)
        self.args = parser.parse_args()
        self.args.data_shape = self.args.data_shape \
            if len(self.args.data_shape) == 2 else self.args.data_shape * 2  # train, test sizes
        if self.args.horovod:
            assert hvd, "You are trying to use horovod support but it's not installed"

    def _get_datasets(self):
        dataset = self.args.dataset
        # root = self.args.root
        save_prefix = self.args.save_prefix
        train_shape, val_shape = self.args.data_shape
        if dataset.lower() == 'voc':
            val_metric = VOC07MApMetric(iou_thresh=0.5, class_names=gdata.VOCDetection.CLASSES)
            train_dataset = gdata.RecordFileDetection('data/trainval.rec', coord_normalized=True)
            val_dataset = gdata.RecordFileDetection('data/test.rec', coord_normalized=True)
        elif dataset.lower() == 'coco':
            train_dataset = gdata.COCODetection(splits='instances_train2017', use_crowd=False)
            val_dataset = gdata.COCODetection(splits='instances_val2017', skip_empty=False)
            val_metric = COCODetectionMetric(
                val_dataset, save_prefix + '_eval', cleanup=True,
                data_shape=(val_shape, val_shape))
        else:
            raise NotImplementedError('Dataset: {} not implemented.'.format(dataset))
        if self.args.num_samples < 0:
            self.args.num_samples = len(train_dataset)
        if self.args.mixup:
            from gluoncv.data import MixupDetection
            train_dataset = MixupDetection(train_dataset)
        return train_dataset, val_dataset, val_metric

    def init(self):
        self._parse_arguments()
        if self.args.amp:
            amp.init()

        if self.args.horovod:
            hvd.init()
        # fix seed for mxnet, numpy and python builtin random generator.
        gutils.random.seed(self.args.seed)

        # training contexts
        if self.args.horovod:
            ctx = [mx.gpu(hvd.local_rank())]
        else:
            ctx = [mx.gpu(int(i)) for i in self.args.gpus.split(',') if i.strip()]
            ctx = ctx if ctx else [mx.cpu()]

        # network
        train_shape, val_shape = self.args.data_shape
        ns = [self.__class__.base_name, str(train_shape), self.args.network]
        if self.args.postfix != '':
            ns.append(self.args.postfix)
        net_name = '+'.join(ns)
        self.args.save_prefix = os.path.join(self.args.save_prefix, net_name)

        if self.args.syncbn and len(ctx) > 1:
            net = self.__class__.model_maker(net_name, pretrained_base=True, norm_layer=gluon.contrib.nn.SyncBatchNorm,
                                             norm_kwargs={'num_devices': len(ctx)})
            async_net = self.__class__.model_maker(net_name, pretrained_base=False)  # used by cpu worker
        else:
            net = self.__class__.model_maker(net_name, pretrained_base=True, norm_layer=gluon.nn.BatchNorm)
            async_net = net

        if self.args.resume.strip():
            net.load_parameters(self.args.resume.strip())
            async_net.load_parameters(self.args.resume.strip())
            net.collect_params().reset_ctx(ctx)
            async_net.collect_params().reset_ctx(ctx)
        else:
            with warnings.catch_warnings(record=True) as w:
                warnings.simplefilter("always")
                net.initialize()
                async_net.initialize()
                # needed for net to be first gpu when using AMP
                net.collect_params().reset_ctx(ctx[0])
        self.net = net
        self.async_net = async_net
        self.ctx = ctx

    def train(self):
        train_dataset, val_dataset, eval_metric = self._get_datasets()
        batch_size = (self.args.batch_size // hvd.size()) if self.args.horovod else self.args.batch_size
        train_data, val_data = self.__class__._make_dataloader(self.async_net, train_dataset, val_dataset,
                                                               batch_size, self.ctx[0], args=self.args)
        self._train_internal(train_data, val_data, eval_metric)

    def validate(self, net, val_data, ctx, eval_metric):
        """Test on validation dataset."""
        eval_metric.reset()
        # set nms threshold and topk constraint
        net.set_nms(nms_thresh=0.45, nms_topk=400)
        mx.nd.waitall()
        if not self.args.debug:
            net.hybridize()
        count = 0
        used_time = 0
        for batch in tqdm.tqdm(val_data, desc='Validating'):
            count += batch[0].shape[0]
            data = gluon.utils.split_and_load(batch[0], ctx_list=ctx, batch_axis=0, even_split=False)
            label = gluon.utils.split_and_load(batch[1], ctx_list=ctx, batch_axis=0, even_split=False)
            det_bboxes = []
            det_ids = []
            det_scores = []
            gt_bboxes = []
            gt_ids = []
            gt_difficults = []
            for x, y in zip(data, label):
                # get prediction results
                b = time.time()
                with mx.autograd.predict_mode():
                    ids, scores, bboxes = net(x)
                mx.nd.waitall()
                used_time += time.time() - b
                det_ids.append(ids)
                det_scores.append(scores)
                # clip to image size
                det_bboxes.append(bboxes.clip(0, batch[0].shape[2]))
                # split ground truths
                gt_ids.append(y.slice_axis(axis=-1, begin=4, end=5))
                gt_bboxes.append(y.slice_axis(axis=-1, begin=0, end=4))
                gt_difficults.append(y.slice_axis(axis=-1, begin=5, end=6) if y.shape[-1] > 5 else None)

            # update metric
            eval_metric.update(det_bboxes, det_ids, det_scores, gt_bboxes, gt_ids, gt_difficults)
        fps = count / used_time
        return eval_metric.get(), fps

    def test(self):
        train_dataset, val_dataset, eval_metric = self._get_datasets()
        batch_size = (self.args.batch_size // hvd.size()) if self.args.horovod else self.args.batch_size
        _, val_data = self.__class__._make_dataloader(self.async_net, train_dataset, val_dataset,
                                                      batch_size, self.ctx[0], args=self.args)
        (map_name, mean_ap), fps = self.validate(self.net, val_data, self.ctx, eval_metric)
        val_msg = '\n'.join(['{}={}'.format(k, v) for k, v in zip(map_name, mean_ap)])
        print(val_msg)
        print(f'Validating FPS: {fps:.2f}')

    def save_params(self, best_map, current_map, epoch):
        save_interval = self.args.save_interval
        prefix = self.args.save_prefix
        current_map = float(current_map)
        if current_map > best_map[0]:
            best_map[0] = current_map
            self.net.save_parameters('{:s}_best.params'.format(prefix, epoch, current_map))
            with open(prefix + '_best_map.log', 'a') as f:
                f.write('{:04d}:\t{:.4f}\n'.format(epoch, current_map))
        if save_interval and epoch % save_interval == 0:
            self.net.save_parameters('{:s}_{:04d}_{:.4f}.params'.format(prefix, epoch, current_map))

    @abc.abstractmethod
    def _train_internal(self, train_data, val_data, eval_metric):
        pass

    @classmethod
    @abc.abstractmethod
    def _make_dataloader(cls, net,
                         train_dataset, val_dataset,
                         batch_size, ctx, args):
        pass

    @abc.abstractmethod
    def _add_additional_arguments(self, parser):
        pass
