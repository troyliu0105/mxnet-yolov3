import mxnet as mx
from mxnet import gluon
from mxnet.gluon import nn
from gluoncv.model_zoo import get_ssd
from gluoncv.model_zoo.ssd import presets
from gluoncv.data import VOCDetection

__all__ = ['get_model']

PRESETS = {
    'mobilenet1.0': {
        300: lambda pretrained=False,
                    pretrained_base=True,
                    **kwargs: get_ssd(name='mobilenet1.0',
                                      base_size=300,
                                      features=['relu22_fwd', 'relu26_fwd'],
                                      filters=[512, 512, 256, 256],
                                      sizes=[21, 45, 99, 153, 207, 261, 315],
                                      ratios=[[1, 2, 0.5]] + [
                                          [1, 2, 0.5, 3, 1.0 / 3]] * 3 + [
                                                 [1, 2, 0.5]] * 2,
                                      steps=[8, 16, 32, 64, 100, 300],
                                      classes=VOCDetection.CLASSES,
                                      dataset='voc',
                                      pretrained=pretrained,
                                      pretrained_base=pretrained_base),
        512: presets.ssd_512_mobilenet1_0_voc
    },
    'vgg16_atrous': {
        300: presets.ssd_300_vgg16_atrous_voc,
        512: presets.ssd_512_vgg16_atrous_voc
    }
}


def get_model(net_name, pretrained_base=True, norm_layer=nn.BatchNorm, norm_kwargs=None, **kwargs):
    _, data_shape, base_name, *_ = net_name.split('+')
    shape = int(data_shape)
    ssd = PRESETS[base_name][shape](pretrained=False, pretrained_base=pretrained_base,
                                    norm_layer=norm_layer, norm_kwargs=norm_kwargs,
                                    **kwargs)
    return ssd
