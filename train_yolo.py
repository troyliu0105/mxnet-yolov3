"""Train YOLOv3 with random shapes."""
import logging
import os
import time

import gluoncv as gcv
import mxnet as mx
import numpy as np
from mxnet import autograd
from mxnet import gluon

gcv.utils.check_version('0.7.0')
from gluoncv.data.batchify import Tuple, Stack, Pad
from gluoncv.data.transforms.presets.yolo import YOLO3DefaultValTransform
from gluoncv.data.dataloader import RandomTransformDataLoader
from gluoncv.utils import LRScheduler, LRSequential

from models import yolo as myolo
from models import IoUFamilyLoss
from utils.base_trainer import TrainerABC

from mxnet.contrib import amp

try:
    import horovod.mxnet as hvd
except ImportError:
    hvd = None


class Yolov3Trainer(TrainerABC):
    base_name = 'yolo3'
    model_maker = myolo.get_model

    # noinspection PyUnboundLocalVariable
    def _train_internal(self, train_data, val_data, eval_metric):
        """Training pipeline"""
        net = self.net
        args = self.args
        ctx = self.ctx
        net.collect_params().reset_ctx(ctx)
        if args.no_wd:
            for k, v in net.collect_params('.*beta|.*gamma|.*bias').items():
                v.wd_mult = 0.0

        if args.label_smooth:
            net._target_generator._label_smooth = True

        if args.lr_decay_period > 0:
            lr_decay_epoch = list(range(args.lr_decay_period, args.epochs, args.lr_decay_period))
        else:
            lr_decay_epoch = [int(i) for i in args.lr_decay_epoch.split(',')]
        lr_decay_epoch = [e - args.warmup_epochs for e in lr_decay_epoch]
        num_batches = args.num_samples // args.batch_size
        lr_scheduler = LRSequential([
            LRScheduler('linear', base_lr=0, target_lr=args.lr,
                        nepochs=args.warmup_epochs, iters_per_epoch=num_batches),
            LRScheduler(args.lr_mode, base_lr=args.lr,
                        nepochs=args.epochs - args.warmup_epochs,
                        iters_per_epoch=num_batches,
                        step_epoch=lr_decay_epoch,
                        step_factor=args.lr_decay, power=2),
        ])

        if args.horovod:
            hvd.broadcast_parameters(net.collect_params(), root_rank=0)
            trainer = hvd.DistributedTrainer(
                net.collect_params(), 'sgd',
                {'wd': args.wd, 'momentum': args.momentum, 'lr_scheduler': lr_scheduler})
        else:
            trainer = gluon.Trainer(
                net.collect_params(), 'sgd',
                {'wd': args.wd, 'momentum': args.momentum, 'lr_scheduler': lr_scheduler},
                kvstore='local', update_on_kvstore=(False if args.amp else None))

        if args.amp:
            amp.init_trainer(trainer)

        # targets
        sigmoid_ce_loss_fn = gluon.loss.SigmoidBinaryCrossEntropyLoss(from_sigmoid=False)
        box_loss_type = args.box_loss
        if box_loss_type == 'mse':
            l1_loss_fn = gluon.loss.L1Loss()
        else:
            iou_loss_fn = IoUFamilyLoss(x1y1x2y2=True, loss_type=box_loss_type)

        # metrics
        obj_metrics = mx.metric.Loss('ObjLoss')
        # center_metrics = mx.metric.Loss('BoxCenterLoss')
        # scale_metrics = mx.metric.Loss('BoxScaleLoss')
        coord_metrics = mx.metric.Loss('CoordLoss')
        cls_metrics = mx.metric.Loss('ClassLoss')

        # set up logger
        logging.basicConfig()
        logger = logging.getLogger()
        logger.setLevel(logging.INFO)
        log_file_path = args.save_prefix + '_train.log'
        log_dir = os.path.dirname(log_file_path)
        if log_dir and not os.path.exists(log_dir):
            os.makedirs(log_dir)
        fh = logging.FileHandler(log_file_path)
        logger.addHandler(fh)
        logger.info(args)
        logger.info('Start training from [Epoch {}]'.format(args.start_epoch))
        best_map = [0]
        for epoch in range(args.start_epoch, args.epochs):
            if args.mixup:
                # TODO(zhreshold): more elegant way to control mixup during runtime
                try:
                    train_data._dataset.set_mixup(np.random.beta, 1.5, 1.5)
                except AttributeError:
                    train_data._dataset._data.set_mixup(np.random.beta, 1.5, 1.5)
                if epoch >= args.epochs - args.no_mixup_epochs:
                    try:
                        train_data._dataset.set_mixup(None)
                    except AttributeError:
                        train_data._dataset._data.set_mixup(None)

            tic = time.time()
            btic = time.time()
            mx.nd.waitall()
            if not args.debug:
                net.hybridize()
                sigmoid_ce_loss_fn.hybridize()
                if box_loss_type == 'mse':
                    l1_loss_fn.hybridize()
                else:
                    iou_loss_fn.hybridize()
            for i, batch in enumerate(train_data):
                batch_size = batch[0].shape[0]
                data = gluon.utils.split_and_load(batch[0], ctx_list=ctx, batch_axis=0)
                # objectness, center_targets, scale_targets, bbox_targets, weights, class_targets
                fixed_targets = [gluon.utils.split_and_load(batch[it], ctx_list=ctx, batch_axis=0) for it in
                                 range(1, 7)]
                gt_boxes = gluon.utils.split_and_load(batch[7], ctx_list=ctx, batch_axis=0)
                sum_losses = []
                obj_losses = []
                # center_losses = []
                # scale_losses = []
                coord_losses = []
                cls_losses = []
                with autograd.record():
                    for ix, x in enumerate(data):
                        # obj_loss, coord_loss, cls_loss = net(x, gt_boxes[ix],
                        #                                     *[ft[ix] for ft in fixed_targets])
                        obj_p, centers_p, scales_p, bbox_p, class_p, \
                        obj_t, centers_t, scales_t, bbox_t, weight_t, class_t, \
                        class_mask_t = net(x, gt_boxes[ix], *[ft[ix] for ft in fixed_targets])

                        # !!!!!!!!!!!!!!!!!! compute loss here !!!!!!!!!!!!!!!!!!
                        denorm = obj_t.shape_array()[1:].prod().astype(obj_t.dtype)
                        denorm_class = class_t.shape_array()[1:].prod().astype(obj_t.dtype)
                        weight_t = weight_t * obj_t
                        hard_obj_t = mx.nd.where(obj_t > 0, mx.nd.ones_like(obj_t), obj_t)
                        new_obj_mask = mx.nd.where(obj_t > 0, obj_t, obj_t >= 0)
                        class_mask = class_mask_t * obj_t

                        obj_loss = sigmoid_ce_loss_fn(obj_p, hard_obj_t, new_obj_mask) * denorm
                        if box_loss_type == 'mse':
                            coord_loss = sigmoid_ce_loss_fn(centers_p, centers_t, weight_t) * denorm * 2 \
                                         + l1_loss_fn(scales_p, scales_t, weight_t) * denorm * 2
                        else:
                            coord_loss = iou_loss_fn(bbox_p, bbox_t, obj_t > 0.) * denorm * 4

                        cls_loss = sigmoid_ce_loss_fn(class_p, class_t, class_mask) * denorm_class

                        # sum_losses.append(obj_loss + center_loss + scale_loss + cls_loss)
                        sum_losses.append(obj_loss + coord_loss + cls_loss)
                        obj_losses.append(obj_loss)
                        # center_losses.append(center_loss)
                        # scale_losses.append(scale_loss)
                        coord_losses.append(coord_loss)
                        cls_losses.append(cls_loss)
                    if args.amp:
                        with amp.scale_loss(sum_losses, trainer) as scaled_loss:
                            autograd.backward(scaled_loss)
                    else:
                        autograd.backward(sum_losses)
                trainer.step(batch_size)
                if not args.horovod or hvd.rank() == 0:
                    obj_metrics.update(0, obj_losses)
                    # center_metrics.update(0, center_losses)
                    # scale_metrics.update(0, scale_losses)
                    coord_metrics.update(0, coord_losses)
                    cls_metrics.update(0, cls_losses)
                    if args.log_interval and not (i + 1) % args.log_interval:
                        name1, loss1 = obj_metrics.get()
                        # name2, loss2 = center_metrics.get()
                        # name3, loss3 = scale_metrics.get()
                        name2, loss2 = coord_metrics.get()
                        name4, loss4 = cls_metrics.get()
                        logger.info(
                            '[Epoch {}][Batch {}], LR: {:.2E}, Speed: {:.3f} samples/sec, {}={:.3f}, {}={:.3f}, '
                            '{}={:.3f}'.format(
                                epoch, i, trainer.learning_rate, args.batch_size / (time.time() - btic), name1, loss1,
                                name2, loss2,
                                # name3, loss3,
                                name4, loss4))
                    btic = time.time()

            if not args.horovod or hvd.rank() == 0:
                name1, loss1 = obj_metrics.get()
                # name2, loss2 = center_metrics.get()
                # name3, loss3 = scale_metrics.get()
                name2, loss2 = coord_metrics.get()
                name4, loss4 = cls_metrics.get()
                logger.info('[Epoch {}] Training cost: {:.3f}, {}={:.3f}, {}={:.3f}, {}={:.3f}'.format(
                    epoch, (time.time() - tic), name1, loss1, name2, loss2,
                    # name3, loss3,
                    name4, loss4))
                if not (epoch + 1) % args.val_interval:
                    # consider reduce the frequency of validation to save time
                    (map_name, mean_ap), fps = self.validate(net, val_data, ctx, eval_metric)
                    val_msg = '\n'.join(['{}={}'.format(k, v) for k, v in zip(map_name, mean_ap)])
                    logger.info('[Epoch {}] Validation: \n{}'.format(epoch, val_msg))
                    logger.info(f'[Epoch {epoch}] Validation fps: {fps:.2f}')
                    current_map = float(mean_ap[-1])
                else:
                    current_map = 0.
                self.save_params(best_map, current_map, epoch)

    @classmethod
    def _make_dataloader(cls, net,
                         train_dataset, val_dataset,
                         batch_size, ctx, args):
        """Get dataloader."""
        train_shape, val_shape = args.data_shape
        batchify_fn = Tuple(*([Stack() for _ in range(7)] + [Pad(axis=0, pad_val=-1) for _ in
                                                             range(1)]))  # stack image, all targets generated
        # 1. 使用 net 预先得到:
        # anchors list: [(1, 1, 3, 2) x nA]
        # offsets list: [(1, 169, 1, 2), (1, 676, 1, 2), (1, 2704, 1, 2)]
        # featmap list: [(1, 1, 13, 13), (1, 1, 26, 26), (1, 1, 52, 52)]
        # 2. 送入 data: (1, 3, 416, 416), label: (M, 6) [xmin, ymin, xmax, ymax, clz, diff]
        # 3. 得到如下 targets
        # img: (1, 3, 416, 416)
        # objectness: (10647, 1)
        # center_targets: (10647, 2)
        # scale_targets: (10647, 2)
        # class_targets: (10647, 20)
        # gt_bboxes: (M, 4)
        if args.no_random_shape:
            train_loader = gluon.data.DataLoader(
                train_dataset.transform(
                    myolo.YOLO3DefaultTrainTransform(train_shape, train_shape, net, mixup=args.mixup)),
                batch_size, True, batchify_fn=batchify_fn, last_batch='rollover', num_workers=args.num_workers,
                timeout=60 * 60)
        else:
            transform_fns = [myolo.YOLO3DefaultTrainTransform(x * 32, x * 32, net, mixup=args.mixup) for x in
                             range(10, 20)]
            train_loader = RandomTransformDataLoader(
                transform_fns, train_dataset, batch_size=batch_size, interval=10, last_batch='rollover',
                shuffle=True, batchify_fn=batchify_fn, num_workers=args.num_workers)
        val_batchify_fn = Tuple(Stack(), Pad(pad_val=-1))
        val_loader = gluon.data.DataLoader(
            val_dataset.transform(YOLO3DefaultValTransform(val_shape, val_shape)),
            batch_size, False, batchify_fn=val_batchify_fn, last_batch='keep', num_workers=args.num_workers)
        return train_loader, val_loader

    def _add_additional_arguments(self, parser):
        parser.add_argument('--box-loss', default='mse',
                            choices=['mse', 'giou', 'diou', 'ciou'], help='Use box loss.')
        parser.add_argument('--label-smooth', action='store_true', help='Use label smoothing.')
        parser.add_argument('--no-random-shape', action='store_true',
                            help='Use fixed size(data-shape) throughout the training, which will be faster '
                                 'and require less memory. However, final model will be slightly worse.')


if __name__ == '__main__':
    trainer = Yolov3Trainer()
    trainer.init()
    if trainer.args.test:
        trainer.test()
    else:
        trainer.train()
